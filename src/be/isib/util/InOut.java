package be.isib.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Saisies à partir du clavier et affichages à l'écran.
 * <p>
 * 
 * @author Mattens JM
 * @version 1.9, 24/05/2011
 */
public class InOut {

    /**
     * Flux d'entrée standard associé au clavier.
     * <p>
     */
//    public static java.io.BufferedReader stdin = new java.io.BufferedReader(
//            new java.io.InputStreamReader(System.in));
    public static java.io.BufferedReader stdin = null;
    static {
        try {
            stdin = new java.io.BufferedReader(
                    new java.io.InputStreamReader(System.in, "ISO-8859-1"));
        } catch (UnsupportedEncodingException ex) {
            throw new IllegalArgumentException("Bad encoding : " + ex);
        }
    }
    
    /**
     * Flux de sortie standard associé à la console.
     * <p>
     */
    public static java.io.PrintStream stdout = new java.io.PrintStream(
            System.out, true);
    /**
     * Flux d'erreur standard associé à la console.
     * <p>
     */
    public static java.io.PrintStream stderr = new java.io.PrintStream(
            System.err, true);

    /**
     * Lit au clavier un entier compris entre des bornes.
     * <p>
     * La lecture est répétée si le format nest pas valide ou si les bornes ne
     * sont pas respectées.
     * <p>
     * 
     * @param min
     *            la plus petite valeur autorisée
     * @param max
     *            la plus grande valeur autorisée
     * @return le <tt>int</tt> saisi.
     */
    public static int readInt(int min, int max) {
        String s = null;
        int n = 0;
        boolean estOK = false;
        do {
            try {
                s = stdin.readLine();
                n = Integer.parseInt(s);
                if (n < min || n > max) {
                    throw new java.lang.RuntimeException("For number " + n);
                }
                estOK = true;
            } catch (java.io.IOException e) {
                System.err.println("Erreur de lecture : " + e);
                System.exit(1);
            } catch (java.lang.NumberFormatException e) {
                System.err.println("Erreur de format de nombre : " + e);
            } catch (java.lang.RuntimeException e) {
                System.err.println("Erreur de bornes : " + e);
            }
        } while (!estOK);
        return n;
    }

    /**
     * Lit au clavier un entier.
     * <p>
     * La lecture est répétée si le format nest pas valide.
     * <p>
     * 
     * @return le <tt>int</tt> saisi.
     */
    public static int readInt() {
        return readInt(Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    /**
     * Lit au clavier un flottant de type <tt>double</tt> compris entre des
     * bornes.
     * <p>
     * La lecture est répétée si le format n'est pas valide ou si les bornes ne
     * sont pas respectées.
     * 
     * @param min
     *            la plus petite valeur autorisée
     * @param max
     *            la plus grande valeur autorisée
     * @return le <tt>double</tt> saisi.
     */
    public static double readDouble(double min, double max) {
        String s = null;
        double d = 0.0d;
        boolean estOK = false;
        do {
            try {
                s = stdin.readLine();
                d = Double.parseDouble(s);
                if (d < min || d > max) {
                    throw new java.lang.RuntimeException("For number " + d);
                }
                estOK = true;
            } catch (java.io.IOException e) {
                System.err.println("Erreur de lecture : " + e);
                System.exit(1);
            } catch (java.lang.NumberFormatException e) {
                System.err.println("Erreur de format de nombre : " + e);
            } catch (java.lang.RuntimeException e) {
                System.err.println("Erreur de bornes : " + e);
            }
        } while (!estOK);
        return d;
    }

    /**
     * Lit au clavier un flottant de type <tt>double</tt>.
     * <p>
     * La lecture est répétée si le format n'est pas valide.
     * 
     * @return le <tt>double</tt> saisi.
     */
    public static double readDouble() {
        return readDouble(-Double.MAX_VALUE, Double.MAX_VALUE);
    }

    /**
     * Lit au clavier un flottant de type <tt>float</tt> compris entre des
     * bornes.
     * <p>
     * La lecture est répétée si le format n'est pas valide ou si les bornes ne
     * sont pas respectées.
     * 
     * @param min
     *            la plus petite valeur autorisée
     * @param max
     *            la plus grande valeur autorisée
     * @return le <tt>float</tt> saisi.
     */
    public static float readFloat(float min, float max) {
        String s = null;
        float fl = 0.0f;
        boolean estOK = false;
        do {
            try {
                s = stdin.readLine();
                fl = Float.parseFloat(s);
                if (fl < min || fl > max) {
                    throw new java.lang.RuntimeException("For number " + fl);
                }
                estOK = true;
            } catch (java.io.IOException e) {
                System.err.println("Erreur de lecture : " + e);
                System.exit(1);
            } catch (java.lang.NumberFormatException e) {
                System.err.println("Erreur de format de nombre : " + e);
            } catch (java.lang.RuntimeException e) {
                System.err.println("Erreur de bornes : " + e);
            }
        } while (!estOK);
        return fl;
    }

    /**
     * Lit au clavier un flottant de type <tt>float</tt>.
     * <p>
     * La lecture est répétée si le format n'est pas valide.
     * 
     * @return le <tt>float</tt> saisi.
     */
    public static float readFloat() {
        return readFloat(-Float.MAX_VALUE, Float.MAX_VALUE);
    }

    /**
     * Affiche le message <tt>"Voulez-vous arrêter <O/N> ?</tt> et renvoie <tt>true</tt> ou <tt>false</tt>
     * suivant que l'on désire arrêter ou non.
     * <p>
     * La lecture est répétée si le caractère introduit n'est pas valide.
     * <p>
     * 
     * @return <tt>true</tt> si on veut arrêter, <tt>false</tt> sinon.
     */
    public static boolean stop() {
        char c = ' ';
        do {
            System.out.print("Voulez-vous arrêter <O/N> ? ");
            try {
                c = (char) stdin.read();
                stdin.read();
//                stdin.read();
            } catch (java.io.IOException e) {
                System.err.println("Erreur de lecture : " + e);
                System.exit(1);
            }
            c = Character.toUpperCase(c);
        } while (c != 'O' && c != 'N');
        return (c == 'O');
    }

    /**
     * LireString lit au clavier une chaîne de caractère.
     * @return la référence à la chaîne de caractères
     */
    public static String readString() {
        String s = null;
        try {
            s = stdin.readLine();
        } catch (IOException e) {
            stderr.println("Erreur de lecture : " + e);
            System.exit(1);
        }
        return s;
    }

    /**
     * LireChar lit au clavier un caractère.
     * @return le caractère lu
     */
    public static char readChar() {
        char c = ' ';
        try {
            c = (char) stdin.read();
            stdin.read();
//            stdin.read();
        } catch (IOException e) {
            stderr.println("Erreur de lecture : " + e);
            System.exit(1);
        }
        return c;
    }
}
