package be.isib.rpg.equipement;

public class Epee extends Equipement {
    public Epee() {
        this.setNom("Epee");
        this.setNiveau(1);
        this.setTypeAttaque(TypeAttaque.CORPS_A_CORPS);
    }
}
