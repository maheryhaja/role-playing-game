package be.isib.rpg.equipement;

public class Baton extends Equipement {
    public Baton() {
        this.setNom("Baton");
        this.setNiveau(1);
        this.setTypeAttaque(TypeAttaque.MAGIQUE);
    }
}
