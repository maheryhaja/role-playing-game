package be.isib.rpg.equipement;

public class Glaive extends Equipement{
    public Glaive() {
        this.setNom("Glaive");
        this.setNiveau(2);
        this.setTypeAttaque(TypeAttaque.CORPS_A_CORPS);
    }

}
