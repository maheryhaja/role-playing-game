package be.isib.rpg.equipement;

public enum TypeAttaque {
    CORPS_A_CORPS, DISTANCE, MAGIQUE
}
