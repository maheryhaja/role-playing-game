/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.isib.rpg.equipement;

import be.isib.rpg.combat.Combat;
import be.isib.rpg.personnage.EquipementSansAttaqueSpecial;
import be.isib.rpg.personnage.Monstre;

public abstract class Equipement implements EquipementSpecial {
    private String nom = null;
    private int niveau = 0;
    private TypeAttaque typeAttaque = null;

    public static Equipement equipementBasNiveau[] = new Equipement[]{new Epee(), new Arc(), new Baton()};
    public static Equipement equipementHautNiveau[] = new Equipement[]{new Glaive(), new Arbalete(), new Sceptre()};
    public static Equipement equipementLegendaire[] = new Equipement[]{new Marteau(), new Tromblon(), new SceptreDeFeu()};

    @Override
    public String toString() {
        return this.nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getNiveau() {
        return niveau;
    }

    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }

    public TypeAttaque getTypeAttaque() {
        return typeAttaque;
    }

    public void setTypeAttaque(TypeAttaque typeAttaque) {
        this.typeAttaque = typeAttaque;
    }

    public int getBoost() {
        int niveau = this.niveau;
        int boost = 0;
        switch (niveau) {
            case 1:
                boost = 2;
                break;
            case 2:
                boost = 4;
                break;
            case 3:
                boost = 5;
                break;
            default:
                boost = 0;
        }
        return boost;
    }

    public String getDescription() {
        String typeArme = "";
        String typeBonus = "";
        switch (this.typeAttaque) {
            case CORPS_A_CORPS:
                typeArme = "arme au Cac";
                typeBonus = "FORCE";
                break;
            case DISTANCE:
                typeArme = "arme à Distance";
                typeBonus = "PRECISION";
                break;
            default:
                typeArme = "arme magique";
                typeBonus = "INTELLIGENCE";
        }
        return this.getNom() + " : " + typeArme + " de niv " + this.niveau + " : Bonus " + typeBonus + " + " + this.getBoost();
    }

    @Override
    public int attaqueSpecial(Monstre ennemi, Combat combat) throws EquipementSansAttaqueSpecial {
        throw new EquipementSansAttaqueSpecial();
    }

    @Override
    public String descriptionDerniereAttaqueSpeciale() {
        return "Aucune attaque spéciale";
    }
}
