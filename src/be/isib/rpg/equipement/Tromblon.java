package be.isib.rpg.equipement;

import be.isib.rpg.combat.Cible;
import be.isib.rpg.combat.Combat;
import be.isib.rpg.combat.Rechargement;
import be.isib.rpg.personnage.Monstre;
import be.isib.rpg.personnage.Personnage;

public class Tromblon extends Equipement {

    private String descriptionDerniereAttaqueSpeciale = "";

    public Tromblon() {
        this.setNom("Tromblon");
        this.setNiveau(3);
        this.setTypeAttaque(TypeAttaque.DISTANCE);
    }

    @Override
    public int attaqueSpecial(Monstre ennemi, Combat combat) {
        int nombreDeCoups = Personnage.randomiser(2, 3);
        combat.infligerAlteration(new Rechargement(), Cible.HERO);
        String description = "Attaque Spéciale (Tromblon): " + ennemi.toString() + " touché " + nombreDeCoups + " fois";
        this.descriptionDerniereAttaqueSpeciale = description;
        System.out.println(description);
        return nombreDeCoups * 5;
    }

    @Override
    public String descriptionDerniereAttaqueSpeciale() {
        return this.descriptionDerniereAttaqueSpeciale;
    }
}
