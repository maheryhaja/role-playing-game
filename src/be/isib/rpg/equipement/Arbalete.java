package be.isib.rpg.equipement;

public class Arbalete extends Equipement {
    public Arbalete() {
        this.setNom("Arbalete");
        this.setNiveau(2);
        this.setTypeAttaque(TypeAttaque.DISTANCE);
    }
}
