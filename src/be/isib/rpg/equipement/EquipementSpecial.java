package be.isib.rpg.equipement;

import be.isib.rpg.combat.Combat;
import be.isib.rpg.personnage.EquipementSansAttaqueSpecial;
import be.isib.rpg.personnage.Monstre;

public interface EquipementSpecial {
     int attaqueSpecial(Monstre ennemi, Combat combat) throws EquipementSansAttaqueSpecial;

     String descriptionDerniereAttaqueSpeciale();
}
