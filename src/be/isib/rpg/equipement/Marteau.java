package be.isib.rpg.equipement;

import be.isib.rpg.combat.Cible;
import be.isib.rpg.combat.Combat;
import be.isib.rpg.combat.Etourdissement;
import be.isib.rpg.combat.Rechargement;
import be.isib.rpg.personnage.Monstre;

public class Marteau extends Equipement {

    private String descriptionDerniereAttaqueSpeciale = "";

    public Marteau() {
        this.setNom("Marteau");
        this.setNiveau(3);
        this.setTypeAttaque(TypeAttaque.CORPS_A_CORPS);
    }

    @Override
    public int attaqueSpecial(Monstre ennemi, Combat combat) {
        Etourdissement etourdissement = new Etourdissement();
        combat.infligerAlteration(etourdissement, Cible.MONSTRE);
        combat.infligerAlteration(new Rechargement(), Cible.HERO);
        String description = "Attaque Spéciale (Marteau):  " + ennemi.toString() + " assomé pour " + etourdissement.getNbTourAssome() + " tours";
        this.descriptionDerniereAttaqueSpeciale = description;
        System.out.println(description);

        return 5;
    }

    @Override
    public String descriptionDerniereAttaqueSpeciale() {
        return this.descriptionDerniereAttaqueSpeciale;
    }
}
