package be.isib.rpg.equipement;

public class Sceptre extends Equipement{
    public Sceptre() {
        this.setNom("Sceptre");
        this.setNiveau(2);
        this.setTypeAttaque(TypeAttaque.MAGIQUE);
    }
}
