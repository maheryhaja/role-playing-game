package be.isib.rpg.equipement;

public class Arc extends Equipement {
    public Arc() {
        this.setNom("Arc");
        this.setNiveau(1);
        this.setTypeAttaque(TypeAttaque.DISTANCE);
    }

}
