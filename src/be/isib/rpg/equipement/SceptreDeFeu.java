package be.isib.rpg.equipement;

import be.isib.rpg.combat.Brulure;
import be.isib.rpg.combat.Cible;
import be.isib.rpg.combat.Combat;
import be.isib.rpg.combat.Rechargement;
import be.isib.rpg.personnage.Monstre;

public class SceptreDeFeu extends Equipement {

    private String descriptionDerniereAttaqueSpeciale = "";

    public SceptreDeFeu() {
        this.setNom("Sceptre de Feu");
        this.setNiveau(3);
        this.setTypeAttaque(TypeAttaque.MAGIQUE);
    }

    @Override
    public int attaqueSpecial(Monstre ennemi, Combat combat) {
        Brulure brulure = new Brulure();
        combat.infligerAlteration(brulure, Cible.MONSTRE);
        combat.infligerAlteration(new Rechargement(), Cible.HERO);
        String description = "Attaque Spéciale (Sceptre de Feu):  " + ennemi.toString() + " brulé pour " + brulure.getNbTourDeBrulure() + " tours.";
        this.descriptionDerniereAttaqueSpeciale = description;
        System.out.println(description);
        return 5;
    }

    @Override
    public String descriptionDerniereAttaqueSpeciale() {
        return this.descriptionDerniereAttaqueSpeciale;
    }
}
