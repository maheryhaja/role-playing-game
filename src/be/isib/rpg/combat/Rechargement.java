package be.isib.rpg.combat;

public class Rechargement implements Alteration {
    @Override
    public void activer(Combat combat, Cible cible) {
        for (Tour tour : combat.prochainsTours(3, true)) {
            tour.getStatut(cible).setRechargement(true);
        }
    }
}
