package be.isib.rpg.combat;

import be.isib.rpg.equipement.Equipement;
import be.isib.rpg.equipement.TypeAttaque;
import be.isib.rpg.personnage.AucunEquipementException;
import be.isib.rpg.personnage.Heros;
import be.isib.rpg.personnage.Monstre;
import be.isib.rpg.personnage.Personnage;

import java.awt.*;
import java.util.ArrayList;

public class Tour {
    private Statut statutHero = new Statut();
    private Statut statutMonstre = new Statut();
    private Combat combat;

    private int degatInflige = 0;
    private int degatRecue = 0;
    private int nbAttaquePortee = 0;
    private int nbAttaqueRecue = 0;


    private ArrayList<TourCallBack> tourCallBacks = new ArrayList<TourCallBack>();

    public void addTourCallBack(TourCallBack callBack) {
        tourCallBacks.add(callBack);
    }

    public void viderTourCallBacks() {
        this.tourCallBacks.clear();
    }

    public Statut getStatut(Cible cible) {
        return cible == Cible.HERO ? this.statutHero : this.statutMonstre;
    }

    public Tour(Combat combat) {
        this.combat = combat;
    }

    public void tourJoueur() {

        tourCallBacks.forEach(tourCallBack -> tourCallBack.debutTourJoueur());
    }

    public void leJoueurAttaqueNormalement(TypeAttaque choixAttaque) {
        Heros hero = this.combat.getHeros();
        Monstre monstre = this.combat.getMonstre();

        int degat;
        switch (choixAttaque) {
            case CORPS_A_CORPS:
                degat = hero.attaquerCaC(monstre);
                break;
            case DISTANCE:
                degat = hero.attaquerDistance(monstre);
                break;
            default:
                degat = hero.attaquerMagie(monstre);
                break;
        }
        this.nbAttaquePortee++;
        this.degatInflige += degat;
        tourCallBacks.forEach(tourCallBack -> tourCallBack.finTourJoueur(this, choixAttaque, this.degatInflige, false, ""));
        this.passerAuTourDuMonstre();
    }

    public void leJoueurEffectueUneAttaqueSpeciale() {

        Heros hero = this.combat.getHeros();
        Monstre monstre = this.combat.getMonstre();
        if (hero.possedeEquipementSpecial() && !this.statutHero.isRechargement()) {

            TypeAttaque choixAttaque = hero.getInventaire()[2].getTypeAttaque();
            int degat = hero.lancerAttaqueSpeciale(monstre, this.combat);
            String descriptionAttaqueSpeciale = hero.getInventaire()[2].descriptionDerniereAttaqueSpeciale();
            this.nbAttaquePortee++;
            this.degatInflige += degat;
            this.tourCallBacks.forEach(tourCallBack -> tourCallBack.finTourJoueur(this, choixAttaque, this.degatInflige, true, descriptionAttaqueSpeciale));
        }
        this.passerAuTourDuMonstre();

    }

    public void leJoueurChoisiLaMeilleureAction() {
        Heros hero = this.combat.getHeros();
        Monstre monstre = this.combat.getMonstre();
        if (hero.possedeEquipementSpecial() && !this.statutHero.isRechargement()) {

            this.leJoueurEffectueUneAttaqueSpeciale();
        } else {
            TypeAttaque attaqueOptimale = hero.attaquerAvecLePlusDeDegat(monstre);
            this.leJoueurAttaqueNormalement(attaqueOptimale);
        }
        this.passerAuTourDuMonstre();

    }

    private void passerAuTourDuMonstre() {
        Monstre monstre = this.combat.getMonstre();
        if (monstre.getPv() > 0) {
            this.tourMonstre();
        } else {
            this.tourCallBacks.forEach(tourCallback -> tourCallback.finDuTour(this));
        }
    }

    public void tourMonstre() {
        Monstre monstre = this.combat.getMonstre();
        Heros hero = this.combat.getHeros();
        int degat = 0, degatSupp = 0;
        this.tourCallBacks.forEach(tourCallback -> tourCallback.debutTourMonstre());
        if (!statutMonstre.isEtourdissement()) {

            if (monstre.possedeAttaqueSpecial() && !statutMonstre.isRechargement()) {
                this.combat.infligerAlteration(new Charge(), Cible.MONSTRE);
                this.combat.infligerAlteration(new Rechargement(), Cible.MONSTRE);
                // prevenir de la preparation attaque speciale
            } else {

                degat = monstre.attaquer(hero);
                if (statutMonstre.isCharge()) {
                    degatSupp = Personnage.randomiser(9, 11);
                    degat += degatSupp;
                    // prevenir que le minotaure relache sa charge
                    hero.setPv(hero.getPv() - degatSupp);
                }

                this.degatRecue += degat;
                this.nbAttaqueRecue++;
                // prevenir des dégats recus
            }

        }

        if (statutMonstre.isBrulure()) {
            this.degatInflige += 2;
            // prevenir que le monstre est brulé et recoit un montant de degats
            monstre.setPv(monstre.getPv() - 2);
        }
        int finalDegatSupp = degatSupp;
        this.tourCallBacks.forEach(tourCallback -> tourCallback.finTourMonstre(this, this.degatRecue, finalDegatSupp));
        this.tourCallBacks.forEach(tourCallback -> tourCallback.finDuTour(this));
    }


    public static String format = "%-35s%-20s%-4s%-10s%-3s\n";

    public void commencer() {
        Heros hero = this.combat.getHeros();
        Monstre monstre = this.combat.getMonstre();

        if (hero.getPv() > 0) {

            System.out.println("Attaque du Hero ");

            int degat;
            TypeAttaque choixAttaque;
            if (hero.possedeEquipementSpecial() && !this.statutHero.isRechargement()) {

                choixAttaque = hero.getInventaire()[2].getTypeAttaque();
                degat = hero.lancerAttaqueSpeciale(monstre, this.combat);

            } else {


                choixAttaque = hero.attaquerAvecLePlusDeDegat(monstre);
                switch (choixAttaque) {
                    case CORPS_A_CORPS:
                        degat = hero.attaquerCaC(monstre);
                        break;
                    case DISTANCE:
                        degat = hero.attaquerDistance(monstre);
                        break;
                    default:
                        degat = hero.attaquerMagie(monstre);
                        break;
                }

            }

            this.nbAttaquePortee++;
            this.degatInflige += degat;
            System.out.printf(format, "Le joueur attaque " + Heros.afficherTypeAttaque(choixAttaque), "Dégâts infligés =", "" + degatInflige, "PV monstre =", monstre.getPv() + "");


        }
        if (monstre.getPv() > 0) {

            if (statutMonstre.isEtourdissement()) {
                System.out.println("Le monstre est étourdi et ne peut pas attaquer");
            }

            if (!statutMonstre.isEtourdissement()) {

                if (monstre.possedeAttaqueSpecial() && !statutMonstre.isRechargement()) {
                    this.combat.infligerAlteration(new Charge(), Cible.MONSTRE);
                    this.combat.infligerAlteration(new Rechargement(), Cible.MONSTRE);
                    System.out.println("Le " + monstre.toString() + " prépare son attaque spéciale");
                } else {

                    int degat = monstre.attaquer(hero);

                    if (statutMonstre.isCharge()) {
                        int degatSupp = Personnage.randomiser(9, 11);
                        degat += degatSupp;
                        System.out.println("Le Minotaure relache sa charge et inflige " + degatSupp + " points de dégat en plus");
                        hero.setPv(hero.getPv() - degatSupp);
                    }

                    this.degatRecue += degat;
                    this.nbAttaqueRecue++;
                    System.out.printf(format, "Le monstre attaque", "Dégâts reçus =", "" + degatRecue, "PV joueur =", hero.getPv());
                }

            }

            if (statutMonstre.isBrulure()) {
                this.degatInflige += 2;
                System.out.println("Le monstre se brule et perd 2PV, PV du monstre: " + monstre.getPv());
                monstre.setPv(monstre.getPv() - 2);
            }
        }

    }

    public int getDegatInflige() {
        return degatInflige;
    }

    public int getDegatRecue() {
        return degatRecue;
    }

    public int getNbAttaquePortee() {
        return nbAttaquePortee;
    }

    public int getNbAttaqueRecue() {
        return nbAttaqueRecue;
    }

    public Monstre getMonstre() {
        return this.combat.getMonstre();
    }

    public Heros getHero() {
        return this.combat.getHeros();
    }
}
