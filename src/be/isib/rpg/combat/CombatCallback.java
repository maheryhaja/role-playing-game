package be.isib.rpg.combat;

import be.isib.rpg.equipement.Equipement;
import be.isib.rpg.personnage.Heros;
import be.isib.rpg.personnage.Monstre;

public interface CombatCallback extends TourCallBack {
    void debutCombat();

    void finCombat(Combat combat, Equipement butin, Heros heros, Monstre monstre, int indexCombat);
}
