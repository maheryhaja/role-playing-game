package be.isib.rpg.combat;

public interface Alteration {
    void activer(Combat combat, Cible cible);
}
