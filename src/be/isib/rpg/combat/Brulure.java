package be.isib.rpg.combat;

import be.isib.rpg.personnage.Personnage;

public class Brulure implements Alteration {

    int nbTourDeBrulure;

    @Override
    public void activer(Combat combat, Cible cible) {
        int toursRandom = Personnage.randomiser(2, 4);
        for (Tour tour : combat.prochainsTours(toursRandom, true)) {
            tour.getStatut(cible).setBrulure(true);
        }
        this.nbTourDeBrulure = toursRandom;
    }

    public int getNbTourDeBrulure() {
        return nbTourDeBrulure;
    }
}
