package be.isib.rpg.combat;

import be.isib.rpg.equipement.TypeAttaque;
import be.isib.rpg.personnage.Heros;
import be.isib.rpg.personnage.Monstre;

import java.util.ArrayList;
import java.util.List;

public class Combat implements TourCallBack {
    private Monstre monstre;
    private Heros heros;
    private List<Tour> tours = new ArrayList<>();
    private int tourIndex = -1;
    private boolean combatTermine = false;
    private ArrayList<CombatCallback> combatCallbacks = new ArrayList<>();

    public void addCombatCallBack(CombatCallback combatCallback) {
        this.combatCallbacks.add(combatCallback);
    }

    public void viderCombatCallBacks() {
        this.combatCallbacks.clear();
    }


    public Combat(Heros heros, Monstre monstre) {
        this.monstre = monstre;
        this.heros = heros;
    }

    private Tour tourActuel() {
        return this.tours.get(this.tourIndex);
    }

    public void commencer() {
        while (!this.combatTermine) {
            this.tourSuivant();
            Tour tour = this.tourActuel();
            tour.commencer();
            this.combatTermine = this.heros.getPv() <= 0 || this.monstre.getPv() <= 0;
        }
    }

    public void commencerProchainTour() {
        this.tourSuivant();
        this.tourActuel().addTourCallBack(this);
        this.tourActuel().tourJoueur();
    }

    public void commencerLeCombat() {
        this.combatCallbacks.forEach(cb -> cb.debutCombat());
        this.commencerProchainTour();
    }

    public void leJoueurAttaqueNormalement(TypeAttaque choixAttaque) {
        this.tourActuel().leJoueurAttaqueNormalement(choixAttaque);
    }

    public void leJoueurEffectueUneAttaqueSpeciale() {
        this.tourActuel().leJoueurEffectueUneAttaqueSpeciale();
    }

    public void leJoueurUtiliseLaMeilleureAlternative() {
        this.tourActuel().leJoueurChoisiLaMeilleureAction();
    }


    private void tourSuivant() {
        this.tourIndex++;
        if (this.tourIndex >= tours.size()) {
            this.tours.add(new Tour(this));
        }
    }

    public boolean isCombatTermine() {
        return combatTermine;
    }

    public void setCombatTermine(boolean combatTermine) {
        this.combatTermine = combatTermine;
    }

    public boolean combatGagne() {
        return false;
    }

    public Tour[] prochainsTours(int nombre, boolean commencerCetour) {
        Tour[] prochainTours = new Tour[nombre];
        for (int i = 1; i <= nombre; i++) {
            int index = i + this.tourIndex - (commencerCetour ? 1 : 0);
            while (index >= this.tours.size()) {
                this.tours.add(new Tour(this));
            }
            prochainTours[i - 1] = this.tours.get(index);
        }
        return prochainTours;
    }

    public void infligerAlteration(Alteration alteration, Cible cible) {
        alteration.activer(this, cible);
    }

    public Monstre getMonstre() {
        return monstre;
    }

    public Heros getHeros() {
        return heros;
    }

    public String[] getRecapitulatif(String butin) {
        int nbAttaquePortee = 0;
        int degatInflige = 0;
        int nbAttaqueRecue = 0;
        int degatRecue = 0;
        for (int i = 0; i < this.tours.size(); i++) {
            nbAttaquePortee += tours.get(i).getNbAttaquePortee();
            degatInflige += tours.get(i).getDegatInflige();
            nbAttaqueRecue += tours.get(i).getNbAttaqueRecue();
            degatRecue += tours.get(i).getDegatRecue();
        }
        return new String[]{"" + nbAttaquePortee, "" + degatInflige, "" + nbAttaqueRecue, "" + degatRecue, butin};
    }

    // implementation tour callback
    @Override
    public void debutTourJoueur() {
        this.combatCallbacks.forEach(cb -> cb.debutTourJoueur());
    }

    @Override
    public void finTourJoueur(Tour tour, TypeAttaque typeAttaque, int degatInflige, boolean attaqueSpeciale, String descriptionAttaqueSpeciale) {
        this.combatCallbacks.forEach(cb -> cb.finTourJoueur(tour, typeAttaque, degatInflige, attaqueSpeciale, descriptionAttaqueSpeciale));
    }

    @Override
    public void debutTourMonstre() {
        this.combatCallbacks.forEach(cb -> cb.debutTourMonstre());
    }

    @Override
    public void finTourMonstre(Tour tour, int degatTotal, int degatSpecial) {
        this.combatCallbacks.forEach(cb -> cb.finTourMonstre(tour, degatTotal, degatSpecial));
    }

    @Override
    public void finDuTour(Tour tour) {
        tour.viderTourCallBacks();
        this.combatTermine = this.heros.getPv() <= 0 || this.monstre.getPv() <= 0;
        if (!this.combatTermine) {
            this.commencerProchainTour();
        } else {
            this.combatCallbacks.forEach(cb -> cb.finCombat(this, tour.getMonstre().getButin(), tour.getHero(), tour.getMonstre(), -1));
        }
        this.combatCallbacks.forEach(cb -> cb.finDuTour(tour));
    }
}
