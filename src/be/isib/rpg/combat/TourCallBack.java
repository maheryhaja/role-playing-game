package be.isib.rpg.combat;

import be.isib.rpg.equipement.TypeAttaque;

public interface TourCallBack {
    void debutTourJoueur();

    void finTourJoueur(Tour tour, TypeAttaque typeAttaque, int degatInflige, boolean attaqueSpeciale, String descriptionAttaqueSpeciale);

    void debutTourMonstre();

    void finTourMonstre(Tour tour, int degatTotal, int degatSpecial);

    void finDuTour(Tour tour);
}
