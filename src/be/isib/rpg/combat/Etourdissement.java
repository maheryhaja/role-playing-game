package be.isib.rpg.combat;

import be.isib.rpg.personnage.Personnage;

public class Etourdissement implements Alteration {

    int nbTourAssome;

    @Override
    public void activer(Combat combat, Cible cible) {
        int tourRandom = Personnage.randomiser(2, 4);
        for (Tour tour : combat.prochainsTours(tourRandom, true)) {
            tour.getStatut(cible).setEtourdissement(true);
        }
        this.nbTourAssome = tourRandom;
    }

    public int getNbTourAssome() {
        return nbTourAssome;
    }
}
