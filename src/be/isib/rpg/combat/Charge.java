package be.isib.rpg.combat;

public class Charge implements Alteration{
    @Override
    public void activer(Combat combat, Cible cible) {
        Tour[] tours = combat.prochainsTours(1, false);
        tours[0].getStatut(cible).setCharge(true);
    }
}
