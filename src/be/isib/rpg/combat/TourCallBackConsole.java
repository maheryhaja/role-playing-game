package be.isib.rpg.combat;

import be.isib.rpg.equipement.TypeAttaque;
import be.isib.rpg.personnage.Heros;
import be.isib.rpg.personnage.Monstre;
import be.isib.rpg.personnage.Personnage;


public class TourCallBackConsole implements TourCallBack {

    public void debutTourMonstre() {
        System.out.println("Attaque du monstre");
    }

    public void finTourMonstre(Tour tour, int degatTotal, int degatSpecial) {
        Statut statutMonstre = tour.getStatut(Cible.MONSTRE);
        Monstre monstre = tour.getMonstre();
        Heros hero = tour.getHero();

        if (statutMonstre.isEtourdissement()) {
            System.out.println("Le monstre est étourdi et ne peut pas attaquer");
        }

        if (!statutMonstre.isEtourdissement()) {

            if (monstre.possedeAttaqueSpecial() && !statutMonstre.isRechargement()) {
                System.out.println("Le " + monstre.toString() + " prépare son attaque spéciale");
            } else {

                if (statutMonstre.isCharge()) {

                    System.out.println("Le Minotaure relache sa charge et inflige " + degatSpecial + " points de dégat en plus");

                }

                System.out.printf(Tour.format, "Le monstre attaque", "Dégâts reçus =", "" + degatTotal, "PV joueur =", hero.getPv());
            }

        }

        if (statutMonstre.isBrulure()) {
            System.out.println("Le monstre se brule et perd 2PV, PV du monstre: " + monstre.getPv());

        }
    }

    public void debutTourJoueur() {
        System.out.println("Attaque du Hero ");
    }

    public void finTourJoueur(Tour tour, TypeAttaque typeAttaque, int degatInflige, boolean attaqueSpeciale, String descriptionAttaqueSpeciale) {
        Statut statutMonstre = tour.getStatut(Cible.MONSTRE);
        Monstre monstre = tour.getMonstre();
        Heros hero = tour.getHero();

        if (attaqueSpeciale) {
            System.out.println(descriptionAttaqueSpeciale);
        }

        System.out.printf(Tour.format, "Le joueur attaque " + Heros.afficherTypeAttaque(typeAttaque), "Dégâts infligés =", "" + degatInflige, "PV monstre =", monstre.getPv() + "");

    }

    public void finDuTour(Tour tour) {

    }
}
