package be.isib.rpg.combat;

public class Statut {
    private boolean etourdissement = false;
    private boolean brulure = false;
    private boolean rechargement = false;
    private boolean charge = false;

    public boolean isEtourdissement() {
        return etourdissement;
    }

    public void setEtourdissement(boolean etourdissement) {
        this.etourdissement = etourdissement;
    }

    public boolean isBrulure() {
        return brulure;
    }

    public void setBrulure(boolean brulure) {
        this.brulure = brulure;
    }

    public boolean isRechargement() {
        return rechargement;
    }

    public void setRechargement(boolean rechargement) {
        this.rechargement = rechargement;
    }

    public boolean isCharge() {
        return charge;
    }

    public void setCharge(boolean charge) {
        this.charge = charge;
    }
}
