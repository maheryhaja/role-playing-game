package be.isib.rpg.partie;

import be.isib.rpg.combat.Combat;
import be.isib.rpg.combat.CombatCallback;
import be.isib.rpg.combat.Tour;
import be.isib.rpg.equipement.Equipement;
import be.isib.rpg.equipement.TypeAttaque;
import be.isib.rpg.personnage.*;

import java.util.ArrayList;
import java.util.Arrays;

public class Partie implements CombatCallback {

    private Heros heros;
    private Monstre[] monstres;
    private int indexCombat = -1;
    private Combat combatEnCours;

    private ArrayList<PartieCallBack> partieCallBacks = new ArrayList<>();

    public void addPartieCallBack(PartieCallBack callBack) {
        this.partieCallBacks.add(callBack);
    }

    public void clearCallbacks() {
        this.partieCallBacks.clear();
    }


    public void initialiser() {
        this.heros = new Heros();
        this.monstres = new Monstre[]{new Gobelin(), new Orc(), new Demon(), new Minotaure()};
        this.partieCallBacks.forEach(cb -> cb.apresInitialisation());
        this.indexCombat = -1;
        this.commencerCombat();
    }

    private void commencerCombat() {
        if (indexCombat >= this.monstres.length - 1) {
            this.partieCallBacks.forEach(cb -> cb.finDeLaPartie(this.heros));
        } else {
            this.indexCombat++;
            this.combatEnCours = new Combat(this.heros, this.monstres[indexCombat]);
            this.combatEnCours.addCombatCallBack(this);
            this.combatEnCours.commencerLeCombat();
            this.partieCallBacks.forEach(cb -> cb.debutCombat(this.combatEnCours));
        }
    }

    public void leJoueurAttaqueNormalement(TypeAttaque choixAttaque) {
        this.combatEnCours.leJoueurAttaqueNormalement(choixAttaque);
    }

    public void leJoueurEffectueUneAttaqueSpeciale() {
        this.combatEnCours.leJoueurEffectueUneAttaqueSpeciale();
    }

    public void leJoueurUtiliseLaMeilleureAlternative() {
        this.combatEnCours.leJoueurUtiliseLaMeilleureAlternative();
    }

    @Override
    public void debutCombat() {
        this.partieCallBacks.forEach(cb -> cb.debutCombat());
    }

    @Override
    public void finCombat(Combat combat, Equipement butin, Heros heros, Monstre monstre, int index) {
        this.partieCallBacks.forEach(cb -> cb.finCombat(combat, butin, heros, monstre, this.indexCombat));
        this.combatEnCours.viderCombatCallBacks();
        this.commencerCombat();
    }

    @Override
    public void debutTourJoueur() {
        this.partieCallBacks.forEach(cb -> cb.debutTourJoueur());
    }

    @Override
    public void finTourJoueur(Tour tour, TypeAttaque typeAttaque, int degatInflige, boolean attaqueSpeciale, String descriptionAttaqueSpeciale) {
        this.partieCallBacks.forEach(cb -> cb.finTourJoueur(tour, typeAttaque, degatInflige, attaqueSpeciale, descriptionAttaqueSpeciale));
    }

    @Override
    public void debutTourMonstre() {
        this.partieCallBacks.forEach(cb -> cb.debutTourMonstre());
    }

    @Override
    public void finTourMonstre(Tour tour, int degatTotal, int degatSpecial) {
        this.partieCallBacks.forEach(cb -> cb.finTourMonstre(tour, degatTotal, degatSpecial));
    }

    @Override
    public void finDuTour(Tour tour) {
        this.partieCallBacks.forEach(cb -> cb.finDuTour(tour));
    }
}
