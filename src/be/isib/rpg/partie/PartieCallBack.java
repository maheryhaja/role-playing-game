package be.isib.rpg.partie;

import be.isib.rpg.combat.Combat;
import be.isib.rpg.combat.CombatCallback;
import be.isib.rpg.personnage.Heros;
import be.isib.rpg.personnage.Monstre;

public interface PartieCallBack extends CombatCallback {
    void apresInitialisation();

    void debutCombat(Combat combat);

    void finDeLaPartie(Heros heros);
}
