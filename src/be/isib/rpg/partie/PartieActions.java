package be.isib.rpg.partie;

import be.isib.rpg.equipement.Equipement;

public interface PartieActions {
    void attaquerCAC();
    void attaquerDistance();
    void attaquerMagie();
    void attaquerSpecial();
    void ramasserEquipement();
    void partieAuomatique();
    Recapitulatif getRecapitulatif();
    Equipement[] getInventaire();
}
