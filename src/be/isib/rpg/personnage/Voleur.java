package be.isib.rpg.personnage;

public class Voleur extends Heros{
    public Voleur() {
        this.setNom("Voleur");
        this.setPv(35);
        this.setDefense(5);
        this.setForce(4);
        this.setPrecision(5);
        this.setIntelligence(2);
    }
}
