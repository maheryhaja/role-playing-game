package be.isib.rpg.personnage;

import be.isib.rpg.equipement.Equipement;

public abstract class Monstre extends Personnage {
    private int attaque;
    private Equipement butin;

    public int attaquer(Personnage ennemi){
        return this.infligerDesDegats(ennemi, this.attaque);
    }

    public int getAttaque() {
        return attaque;
    }

    public void setAttaque(int attaque) {
        this.attaque = attaque;
    }

    public Equipement getButin() {
        return butin;
    }

    public void setButin(Equipement butin) {
        this.butin = butin;
    }

    protected Equipement choisirEquipementAleatoire(Equipement[] choix) {
        return choix[(int) Math.round(Math.random() * (choix.length - 1))];
    }

    public boolean possedeAttaqueSpecial(){
        return  false;
    }


}
