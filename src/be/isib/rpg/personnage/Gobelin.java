package be.isib.rpg.personnage;

import be.isib.rpg.equipement.Equipement;

public class Gobelin extends Monstre{
    public Gobelin() {
        this.setNom("Gobelin");
        this.setPv(5);
        this.setDefense(0);
        this.setAttaque(2);
        this.setButin(this.choisirEquipementAleatoire(Equipement.equipementBasNiveau));
    }
}
