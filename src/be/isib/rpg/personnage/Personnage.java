package be.isib.rpg.personnage;

/**
 *
 * @author SylvainHuraux
 */
public abstract class Personnage {
    protected int basepv;
    private int pv;
    private int defense;
    protected String nom = null;

    public static int randomiser(int min, int max) {
        return (int) Math.round(min + Math.random() * (max - min));
    }

    /**
     * infliger des degats à la cible
     * @param ennemi
     * @param degat: degat non randomisé
     * @return
     */
    protected int infligerDesDegats(Personnage ennemi, int degat){
        int degatRandomise = degat + randomiser(-2, 2);

        int reductionDeDegatRandomise = ennemi.getDefense() + randomiser(-2, 2);
        int degatInflige = degatRandomise - reductionDeDegatRandomise;
        if (degatInflige < 0) {
            degatInflige = 0;
        }
        ennemi.setPv(ennemi.getPv() - degatInflige);
        int resteDegat = ennemi.getPv() < degatInflige ? ennemi.getPv() : degatInflige;
        if (ennemi.getPv() < 0) {
            ennemi.setPv(0);
        }
        return degatInflige;
    }

    public int getPv() {
        return pv > 0 ? pv : 0;
    }

    public void setPv(int pv) {
        this.pv = pv;
    }

    public int getDefense() {
        return defense;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return this.nom;
    }
}
