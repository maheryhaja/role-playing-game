package be.isib.rpg.personnage;

import be.isib.rpg.equipement.Equipement;

public class Orc extends Monstre{
    public Orc() {
        this.setNom("Orc");
        this.setPv(10);
        this.setDefense(2);
        this.setAttaque(3);
        this.setButin(this.choisirEquipementAleatoire(Equipement.equipementHautNiveau));
    }
}
