package be.isib.rpg.personnage;

public class Guerrier extends Heros{
    public Guerrier() {
        this.setNom("Guerrier");
        this.setPv(35);
        this.setDefense(5);
        this.setForce(5);
        this.setPrecision(4);
        this.setIntelligence(2);
    }
}
