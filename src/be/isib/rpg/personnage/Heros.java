/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.isib.rpg.personnage;

import be.isib.rpg.combat.Combat;
import be.isib.rpg.equipement.Equipement;
import be.isib.rpg.equipement.TypeAttaque;

/**
 * @author SylvainHuraux
 */
public class Heros extends Personnage {
    int force;
    int precision;
    int intelligence;

    Equipement[] inventaire = new Equipement[3];
    String recapCombat[][] = new String[4][5];


    public Heros() {
        this.setPv(30);
        this.basepv = 30;
        this.setDefense(5);
        this.force = 1;
        this.precision = 1;
        this.intelligence = 1;
    }


    public void seSoigner() {
        System.out.printf("Le joueur regagne tous ses PV.");
        this.setPv(this.basepv);
    }

    public Equipement[] getInventaire() {
        return inventaire;
    }

    private int evaluerBonusEquipement(TypeAttaque typeAttaque) {
        int boost = 0;
        for (int i = 0; i < this.inventaire.length; i++) {
            Equipement equipement = this.inventaire[i];
            if (equipement != null) {
                if (equipement.getTypeAttaque() == typeAttaque) {
                    boost = equipement.getBoost();
                }
            }
        }
        return boost;
    }

    private int evaluerDegatCac() {
        return this.force + evaluerBonusEquipement(TypeAttaque.CORPS_A_CORPS);
    }

    private int evaluerDegatDistance() {
        return this.precision + evaluerBonusEquipement(TypeAttaque.DISTANCE);
    }

    private int evaluerDegatMagie() {
        return this.intelligence + evaluerBonusEquipement(TypeAttaque.MAGIQUE);
    }

    public int attaquerCaC(Monstre ennemi) {

        return this.infligerDesDegats(ennemi, this.evaluerDegatCac());
    }

    public int attaquerDistance(Monstre ennemi) {
        return this.infligerDesDegats(ennemi, this.evaluerDegatDistance());
    }

    public int attaquerMagie(Monstre ennemi) {
        return this.infligerDesDegats(ennemi, this.evaluerDegatMagie());
    }


    private int max(int a, int b, int c) {
        int max = a > b ? a : b;
        return max > c ? max : c;
    }

    public int lancerAttaqueSpeciale(Monstre monstre, Combat combat) {
        try {
            Equipement armeSpeciale = inventaire[2];
            int degatSpecial = armeSpeciale.attaqueSpecial(monstre, combat);
            int stab;
            switch (armeSpeciale.getTypeAttaque()){
                case DISTANCE: stab = precision; break;
                case MAGIQUE: stab = intelligence; break;
                default: stab = force;
            }
            return this.infligerDesDegats(monstre, degatSpecial + stab);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public boolean possedeEquipementSpecial(){
        return this.inventaire[2] != null;
    }

    public TypeAttaque attaquerAvecLePlusDeDegat(Monstre ennemi) {
        int degatCac = this.evaluerDegatCac();
        int degatDistance = this.evaluerDegatDistance();
        int degatMagique = this.evaluerDegatMagie();
        int degatMax = this.max(degatCac, degatDistance, degatMagique);

        if (degatMax == degatCac) {
            return TypeAttaque.CORPS_A_CORPS;
        }
        if (degatMax == degatDistance) {
            return TypeAttaque.DISTANCE;
        }
        return TypeAttaque.MAGIQUE;

    }

    public static Heros upgrader(Heros heroDebase, Equipement equipement) {
        Heros heroUpgrade;


        switch (equipement.getTypeAttaque()) {
            case CORPS_A_CORPS:
                heroUpgrade = new Guerrier();
                break;
            case DISTANCE:
                heroUpgrade = new Voleur();
                break;
            default:
                heroUpgrade = new Mage();
        }
        heroUpgrade.inventaire = heroDebase.inventaire;
        heroUpgrade.recapCombat = heroDebase.recapCombat;
        heroUpgrade.attacherEquipement(equipement);
        if (equipement.getNiveau() == 1) {
            heroUpgrade.setPv(heroDebase.getPv() + 5);
            heroUpgrade.basepv += 5;
        }
        return heroUpgrade;
    }


    public void attacherEquipement(Equipement equipement) {
        this.inventaire[equipement.getNiveau() - 1] = equipement;
    }

    public Equipement ramasserButin(Monstre ennemi) throws AucunEquipementException {
        Equipement butin = ennemi.getButin();
        if (butin == null) {
            throw new AucunEquipementException();
        }
        this.attacherEquipement(butin);
        return butin;
    }

    public static String afficherTypeAttaque(TypeAttaque typeAttaque) {
        String attaqueAffiche = "";
        switch (typeAttaque) {
            case CORPS_A_CORPS:
                attaqueAffiche = "au Cac";
                break;
            case DISTANCE:
                attaqueAffiche = "à Distance";
                break;
            default:
                attaqueAffiche = "par Magie";
        }
        return attaqueAffiche;
    }


    public String generePointille(int nombre) {
        String points = "|";
        for (int i = 0; i < nombre - 1; i++) {
            points += "-";
        }
        return points;
    }

    String format = "%-20s%-20s%-20s%-20s%-20s%-20s%s\n";

    private void tracerLigne() {
        System.out.printf(format, generePointille(20), generePointille(20), generePointille(20), generePointille(20), generePointille(20), generePointille(20), "|");
    }

    public void montrerRecapitulatif() {
        tracerLigne();
        System.out.printf(format, "| Monstre combattu", "| Nb d\'atq portes", "| Degats infliges", "| Nb d\'atq recues", "| Degats recus", "| Objet ramassé", "|");
        tracerLigne();
        System.out.printf(format, "| Gobelin", "| " + this.recapCombat[0][0], "| " + this.recapCombat[0][1], "| " + this.recapCombat[0][2], "| " + this.recapCombat[0][3], "| " + this.recapCombat[0][4], "|");
        tracerLigne();
        System.out.printf(format, "| Orc", "| " + this.recapCombat[1][0], "| " + this.recapCombat[1][1], "| " + this.recapCombat[1][2], "| " + this.recapCombat[1][3], "| " + this.recapCombat[1][4], "|");
        tracerLigne();
        System.out.printf(format, "| Demon", "| " + this.recapCombat[2][0], "| " + this.recapCombat[2][1], "| " + this.recapCombat[2][2], "| " + this.recapCombat[2][3], "| " + this.recapCombat[2][4], "|");
        tracerLigne();
        System.out.printf(format, "| Minotaure", "| " + this.recapCombat[3][0], "| " + this.recapCombat[3][1], "| " + this.recapCombat[3][2], "| " + this.recapCombat[3][3], "| " + this.recapCombat[3][4], "|");
        tracerLigne();
    }

    public void pushRecapitulatif(String[] recap, int ligne) {
        this.recapCombat[ligne] = recap;
    }

    public int getForce() {
        return force;
    }

    public void setForce(int force) {
        this.force = force;
    }

    public int getPrecision() {
        return precision;
    }

    public void setPrecision(int precision) {
        this.precision = precision;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }
}
