package be.isib.rpg.personnage;

public class Mage extends Heros{
    public Mage() {
        this.setNom("Mage");
        this.setPv(35);
        this.setDefense(5);
        this.setForce(2);
        this.setPrecision(4);
        this.setIntelligence(5);
    }
}
