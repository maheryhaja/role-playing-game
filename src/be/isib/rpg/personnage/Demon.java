package be.isib.rpg.personnage;

import be.isib.rpg.equipement.Equipement;

public class Demon extends Monstre{
    public Demon() {
        this.setNom("Demon");
        this.setPv(20);
        this.setDefense(5);
        this.setAttaque(5);
        this.setButin(this.choisirEquipementAleatoire(Equipement.equipementLegendaire));
    }
}
