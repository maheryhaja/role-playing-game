package be.isib.rpg.personnage;

public class Minotaure extends Monstre{

    public Minotaure(){
        this.setNom("Minotaure");
        this.setPv(35);
        this.setDefense(8);
        this.setAttaque(8);
    }

    @Override
    public boolean possedeAttaqueSpecial() {
        return true;
    }

}
