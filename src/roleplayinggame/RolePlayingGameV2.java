package roleplayinggame;

import be.isib.rpg.combat.Cible;
import be.isib.rpg.combat.Combat;
import be.isib.rpg.combat.Statut;
import be.isib.rpg.combat.Tour;
import be.isib.rpg.equipement.Equipement;
import be.isib.rpg.equipement.TypeAttaque;
import be.isib.rpg.partie.Partie;
import be.isib.rpg.partie.PartieCallBack;
import be.isib.rpg.personnage.AucunEquipementException;
import be.isib.rpg.personnage.Heros;
import be.isib.rpg.personnage.Monstre;
import be.isib.util.InOut;

import java.util.concurrent.CompletableFuture;

public class RolePlayingGameV2 implements PartieCallBack {

    private Partie partie;
    private boolean gameFinished = false;

    private CompletableFuture<Boolean> futureGameFinished = new CompletableFuture<>();

    public static void main(String[] args) throws InterruptedException {
        RolePlayingGameV2 game = new RolePlayingGameV2();
        game.start();
        while (!game.futureGameFinished.isDone()) {
            Thread.sleep(10000);
        }

    }

    public void start() {
        this.partie = new Partie();
        this.partie.addPartieCallBack(this);
        this.partie.initialiser();
    }

    @Override
    public void debutCombat() {
        System.out.println("debut du combat");
    }

    @Override
    public void finCombat(Combat combat, Equipement butinDuMonstre, Heros hero, Monstre monstre, int indexCombat) {
        String butinString = "Aucun";
        try {
            Equipement equipementRamassee = hero.ramasserButin(monstre);
            butinString = equipementRamassee.toString();
            System.out.println(monstre + " vaincu. Il reste au héros " + hero.getPv() + " PV. Objet récupéré : " + equipementRamassee.getDescription());
            if (indexCombat == 0) {
                hero = Heros.upgrader(hero, equipementRamassee);
                System.out.println("Le joueur devient un " + hero + ". Ses PV sont maintenant = " + hero.getPv());
            }

            if (indexCombat == 2) {

                hero.seSoigner();
                hero = Heros.upgrader(hero, equipementRamassee);
                System.out.println("Le joueur devient un " + hero + ". Ses PV sont maintenant = " + hero.getPv());
            }
        } catch (AucunEquipementException e) {
            System.out.println(monstre + " vaincu. Il reste au héros " + hero.getPv() + " PV. Objet récupéré : Aucun");
        }

        System.out.println("");
        hero.pushRecapitulatif(combat.getRecapitulatif(butinString), indexCombat);
    }

    @Override
    public void debutTourJoueur() {
        System.out.println("Attaque du Hero ");
        this.partie.leJoueurUtiliseLaMeilleureAlternative();
    }

    @Override
    public void finTourJoueur(Tour tour, TypeAttaque typeAttaque, int degatInflige, boolean attaqueSpeciale, String descriptionAttaqueSpeciale) {
        Statut statutMonstre = tour.getStatut(Cible.MONSTRE);
        Monstre monstre = tour.getMonstre();
        Heros hero = tour.getHero();

        if (attaqueSpeciale) {
            System.out.println(descriptionAttaqueSpeciale);
        }

        System.out.printf(Tour.format, "Le joueur attaque " + Heros.afficherTypeAttaque(typeAttaque), "Dégâts infligés =", "" + degatInflige, "PV monstre =", monstre.getPv() + "");

    }

    @Override
    public void debutTourMonstre() {
        System.out.println("Attaque du monstre");

    }

    @Override
    public void finTourMonstre(Tour tour, int degatTotal, int degatSpecial) {
        Statut statutMonstre = tour.getStatut(Cible.MONSTRE);
        Monstre monstre = tour.getMonstre();
        Heros hero = tour.getHero();

        if (statutMonstre.isEtourdissement()) {
            System.out.println("Le monstre est étourdi et ne peut pas attaquer");
        }

        if (!statutMonstre.isEtourdissement()) {

            if (monstre.possedeAttaqueSpecial() && !statutMonstre.isRechargement()) {
                System.out.println("Le " + monstre.toString() + " prépare son attaque spéciale");
            } else {

                if (statutMonstre.isCharge()) {

                    System.out.println("Le Minotaure relache sa charge et inflige " + degatSpecial + " points de dégat en plus");

                }

                System.out.printf(Tour.format, "Le monstre attaque", "Dégâts reçus =", "" + degatTotal, "PV joueur =", hero.getPv());
            }

        }

        if (statutMonstre.isBrulure()) {
            System.out.println("Le monstre se brule et perd 2PV, PV du monstre: " + monstre.getPv());

        }
    }

    @Override
    public void finDuTour(Tour tour) {
        System.out.println("fin du tour");
    }

    @Override
    public void apresInitialisation() {
        System.out.println("Création du héros");
    }

    @Override
    public void debutCombat(Combat combat) {

    }

    @Override
    public void finDeLaPartie(Heros heros) {
        this.partie.clearCallbacks();
        if (heros.getPv() > 0) {
            System.out.println("Le joueur a gagné");

        } else {
            System.out.println("Le joueur a perdu");

        }
        heros.montrerRecapitulatif();

        if (!InOut.stop()) {
            this.start();
        } else {
            this.futureGameFinished.complete(true);
        }
    }
}
