package roleplayinggame;

import be.isib.rpg.combat.Combat;
import be.isib.rpg.equipement.Arbalete;
import be.isib.rpg.equipement.Epee;
import be.isib.rpg.equipement.Equipement;
import be.isib.rpg.personnage.*;
import be.isib.util.InOut;

import java.util.Arrays;


public class RolePlayingGame {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        do {
            Heros hero = new Heros();
            Monstre monstres[] = {new Gobelin(), new Orc(), new Demon()};
            int indexCombat = 0;

            System.out.println("Création du héros");

            while (indexCombat < monstres.length && hero.getPv() > 0) {
                Monstre monstre = monstres[indexCombat];
                Combat combat = new Combat(hero, monstre);
                combat.commencer();

                String butin = "aucun";
                try {
                    Equipement equipementRamassee = hero.ramasserButin(monstre);
                    butin = equipementRamassee.toString();
                    System.out.println(monstre + " vaincu. Il reste au héros " + hero.getPv() + " PV. Objet récupéré : " + equipementRamassee.getDescription());
                    if (indexCombat == 0) {
                        hero = Heros.upgrader(hero, equipementRamassee);
                        System.out.println("Le joueur devient un " + hero + ". Ses PV sont maintenant = " + hero.getPv());
                    }

                    if(indexCombat == 2){

                        hero.seSoigner();
                        hero = Heros.upgrader(hero, equipementRamassee);
                        System.out.println("Le joueur devient un " + hero + ". Ses PV sont maintenant = " + hero.getPv());
                    }
                } catch (AucunEquipementException e) {
                    System.out.println(monstre + " vaincu. Il reste au héros " + hero.getPv() + " PV. Objet récupéré : Aucun");
                }
                hero.pushRecapitulatif(combat.getRecapitulatif(butin), indexCombat);

                indexCombat++;
                System.out.println("");
            }

            Combat combatMinotaure = new Combat(hero, new Minotaure());
            combatMinotaure.commencer();
            hero.pushRecapitulatif(combatMinotaure.getRecapitulatif("Aucun"), indexCombat);
            indexCombat++;

            if (hero.getPv() > 0) {
                System.out.println("Le joueur a gagné");
            } else {
                System.out.println("Le joueur a perdu");
            }

            while (indexCombat < 4) {
                hero.pushRecapitulatif(new String[]{"" + 0, "" + 0, "" + 0, "" + 0, "Aucun"}, indexCombat);
                indexCombat++;

            }
            hero.montrerRecapitulatif();

        } while (!InOut.stop());

    }

}
